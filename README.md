## Surf城市边缘

###产品概括
- surf城市边缘是一款面向户外爱好者的记录分享APP，无论是初步涉足户外还是仅仅喜欢看旅游照片，都可以使用这款app。
- 分为 [首页] / [活动] / [个人] 三大板块
- 其主打特色为“随时分享”“随时评论”

### 用户画像

![用户画像](https://images.gitee.com/uploads/images/2019/0621/030701_f89ec76e_2664783.png "用户画像.png")

#### 功能框架
![输入图片说明](https://images.gitee.com/uploads/images/2019/0621/133914_3a041187_2664783.png "surf+城市边缘.png")

---

## 产品目的
* 城市边缘是追求以新颖简洁且富具美感的app,目的是为满足户外爱好者及旅游攻略爱好者的需求。

---

## 设计规范

###色彩规范
#### 主色调

<table><tr><td bgcolor=#788BAB>#788BAB</td></tr></table>

![788bab](https://images.gitee.com/uploads/images/2019/0621/140320_5a65599a_2664783.png "788BAB.png")

#### 辅色调

<table>><tr><td bgcolor=#797979>#797979</td></tr></table>

![797979](https://images.gitee.com/uploads/images/2019/0621/140450_b4beaf33_2664783.png "797979.png")

<table><tr><td bgcolor=#B2B2B2>#B2B2B2</td></tr></table>

![b2b2b2](https://images.gitee.com/uploads/images/2019/0621/140501_5779da08_2664783.png "b2b2b2.png")

---

### 图标规范

#### 导航图标

#### 采用的是面性图标

图标|图标|图标
--|:--:|--:
![输入图片说明](https://images.gitee.com/uploads/images/2019/0621/041311_21a6d0f4_2664783.png "u63.png") |![输入图片说明](https://images.gitee.com/uploads/images/2019/0621/041323_983950e2_2664783.png "u65.png")|![输入图片说明](https://images.gitee.com/uploads/images/2019/0621/041340_a56f96ec_2664783.png "u66.png")

---

页面组件|页面组件|页面组件
--|:--:|--:
![输入图片说明](https://images.gitee.com/uploads/images/2019/0621/041403_f8ec3d0d_2664783.png "u88.png")|![输入图片说明](https://images.gitee.com/uploads/images/2019/0621/042124_34bf1cf3_2664783.png "u188.png")|![输入图片说明](https://images.gitee.com/uploads/images/2019/0621/041415_f329e478_2664783.png "u89.png")   
![输入图片说明](https://images.gitee.com/uploads/images/2019/0621/042137_a394e08d_2664783.png "u119.png")|![输入图片说明](https://images.gitee.com/uploads/images/2019/0621/041427_f5050601_2664783.png "u90.png")|![输入图片说明](https://images.gitee.com/uploads/images/2019/0621/042159_09f2c124_2664783.png "u109.png")

- 采用了不透明度图标，通过调节图标的部分面性结构来增加层次感


---

### 文字规范

字号|位置|大小
--|:--:|--:
华文细黑|活动页|15
宋体    |活动页|18
Arial|首页 活动页 个人页|12


#### 原型展示
![logo](https://images.gitee.com/uploads/images/2019/0621/035222_932262f4_2664783.png "logo.png")
![引导页](https://images.gitee.com/uploads/images/2019/0621/033909_fd233df8_2664783.png "引导页.png")
![首页](https://images.gitee.com/uploads/images/2019/0621/141742_44c8f227_2664783.png "首页.png")
![活动页](https://images.gitee.com/uploads/images/2019/0621/141820_62cf329d_2664783.png "活动页.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0621/141841_00d0d2f4_2664783.png "个人页.png")
